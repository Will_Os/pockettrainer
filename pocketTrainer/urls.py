"""pocketTrainer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from appPocketTrainer import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('registro/',views.registro, name="registro"),
    path('login/',views.login, name="login"),
    path('logout/',views.logout, name="logout"),
    path('recuperar/',views.recuperarPassword, name="recuperar"),
    path('nuevapass/<email>/<llave>/',views.nuevaPass,name="nuevapass"),
    path('administrar/',views.admninistracion, name="administrar"),
    path('rutina/',views.nuevaRutina, name="rutina"),
    path('cliente/',views.paginaCliente, name="cliente"),
    path('eliminar/<idrutina>/',views.eliminarRutina,name="eliminar"),
    path('mirutina/<idrutina>/',views.miRutina,name="mirutina"),
    path('misdatos/',views.misDatos,name="misdatos")
]
