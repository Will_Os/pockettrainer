var rutina = {};
const rutinas = [];

function addTabla(){
    if($('#duracion').val() == ''){
        $("#errorDuracion").text("Ingrese una duración a su rutina");
    }else{
        var rutina = new Object();
        rutina.ejercicio = $('#ejercicio option:selected').text();
        rutina.duracion = $('#duracion').val();
        rutina.index = $('#ejercicio option:selected').val();
        $('#ejercicio option:selected').remove();
        $("#errorTabla").text("");
        $("#errorDuracion").text("");
        if($('#ejercicio option').length == 0){
            $('#addEjercicio').prop('disabled',true);
        }
        rutinas.push(rutina);
        actualizarTabla();
    }
}

function actualizarTabla(){
    $('.elementoTabla').remove();
    for(i=0;i<rutinas.length;i++){
        $('#tablaEjercicios').append('<tr class="elementoTabla"><th style="display: none;"><input type="text" value="'+rutinas[i].index+'" name=id_ejercicio readonly="true"><input type="text" value="'+rutinas[i].duracion+'" name=duracion_ejercicio readonly="true"></th><th>'+rutinas[i].ejercicio+'</th><th>'+rutinas[i].duracion+'</th><th><button type="button" onclick="eliminarTabla('+i+')">Eliminar</button></th></tr>');
    }
}

function eliminarTabla(i){
    $('#ejercicio').append('<option value="'+rutinas[i].index+'">'+rutinas[i].ejercicio+'</option>');
    $('#addEjercicio').prop('disabled',false);
    rutinas.splice(i,1);
    actualizarTabla();
}

$(document).ready(function() {
    $("#formAddRutina").submit(function(e){
        if(rutinas.length==0){
            e.preventDefault();
            $("#errorTabla").text("Añada como mínimo un ejercicio a su rutina");
        }
    });
});