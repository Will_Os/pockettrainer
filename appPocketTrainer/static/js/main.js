$().ready(function(){
    jQuery.validator.addMethod("alfanumerico", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9_]+$/i.test(value);
    }, "Sólamente letras, números y guiones bajos");
    
    jQuery.validator.addMethod("alfanumail", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9_.@]+$/i.test(value);
    }, "Sólamente letras, números, guiones bajos o mails");

    $('#formRegistro').validate({
        errorClass: "invalido",
        rules:{
            nombre:{
                required: true,
                alfanumerico: true
            },
            apP:{
                required: true,
                alfanumerico: true
            },
            apM:{
                required: true,
                alfanumerico: true
            },
            username:{
                required: true,
                minlength: 4,
                alfanumerico: true
            },
            password1:{
                required: true,
                minlength: 8,
                alfanumerico: true
            },
            password2:{
                required: true,
                minlength: 8,
                equalTo: "#id_password1",
                alfanumerico: true
            },
            email:{
                required: true,
                email: true
            },
            repmail:{
                required: true,
                email: true,
                equalTo: "#id_email"
            }
        },
        messages:{
            nombre:{
                required: "Campo requerido"
            },
            apP:{
                required: "Campo requerido"
            },
            apM:{
                required: "Campo requerido"
            },
            username:{
                required: "Campo requerido",
                minlength: "4 carácteres mínimo"
            },
            password1:{
                required: "Campo requerido",
                minlength: "8 carácteres mínimo"
            },
            password2:{
                required: "Campo requerido",
                minlength: "8 carácteres mínimo",
                equalTo: "Ambas contraseñas deben ser iguales"
            },
            email:{
                required: "Campo requerido",
                email: "Mail inválido"
            },
            repmail:{
                required: "Campo requerido",
                email: "Mail inválido",
                equalTo: "Ambos mail deben ser iguales"
            }
        }
    })
    $('#formFisico').validate({
        errorClass: "invalido",
        rules:{
            capacidadFisica:{
                required: true,
                alfanumerico: true
            },
            peso:{
                required: true,
                number: true
            },
            estatura:{
                required: true,
                number: true
            }
        },
        messages:{
            capacidadFisica:{
                required: "Campo requerido"
            },
            peso:{
                required: "Campo requerido",
                number: "Introduzca un numero válido",
                step: "Introduzca un numero de un solo decimal"
            },
            estatura:{
                required: "Campo requerido",
                number: "Introduzca un número válido",
                step: "Introduzca un numero de solo dos decimales"
            }
        }
    })
    $('#formLogin').validate({
        errorClass: "invalido",
        rules:{
            credencial:{
                required: true,
                minlength: 4,
                alfanumail: true
            },
            password:{
                required: true,
                minlength: 8,
                alfanumail: true
            }
        },
        messages:{
            credencial:{
                required: "Campo requerido",
                minlength: "4 carácteres mínimo"
            },
            password:{
                required: "Campo requerido",
                minlength: "8 carácteres mínimo"
            }
        }
    });
    $('#formCambiar').validate({
        errorClass: "invalido",
        rules:{
            password1:{
                required: true,
                minlength: 8,
                alfanumerico: true
            },
            password2:{
                required: true,
                minlength: 8,
                equalTo: "#password1",
                alfanumerico: true
            }
        },
        messages:{
            password1:{
                required: "Campo requerido",
                minlength: "8 carácteres mínimo"
            },
            password2:{
                required: "Campo requerido",
                minlength: "8 carácteres mínimo",
                equalTo: "Ambas contraseñas deben ser iguales"
            }
        }
    });
    $('#formAddRutina').validate({
        errorClass: "invalido",
        rules:{
            nombre:{  
                required: true,
                minlength: 4,
                alfanumerico: true
            },
            dias:{
                required: true
            }
        },
        messages:{
            nombre:{
                required: "Campo requerido",
                minlength: "4 carácteres mínimo"
            },
            dias:{
                required: "Se debe seleccionar como mínimo un día"
            }
        },
        errorPlacement: function(error, element){
            if(element.attr("type")=='checkbox'){
                error.appendTo($('#errorDias'));
            }else{
                error.insertAfter(element);
            }
        }
    });
});