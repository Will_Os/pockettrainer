from django.apps import AppConfig


class ApppockettrainerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appPocketTrainer'
