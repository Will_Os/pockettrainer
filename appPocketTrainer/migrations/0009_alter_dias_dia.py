# Generated by Django 3.2.1 on 2021-06-27 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appPocketTrainer', '0008_perfilrutina_nombre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dias',
            name='dia',
            field=models.CharField(choices=[(0, 'Lunes'), (1, 'Martes'), (2, 'Miercoles'), (3, 'Jueves'), (4, 'Viernes'), (5, 'Sabado'), (6, 'Domingo')], max_length=128),
        ),
    ]
