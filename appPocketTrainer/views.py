#Recursos
from django.shortcuts import render, redirect
from appPocketTrainer.models import *
from django.contrib.auth.models import User, Group
from django.utils.safestring import mark_safe
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import logout as funcLogout
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import send_mail
from cryptography.fernet import Fernet
import base64
import datetime

#Clases
class crearForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1','password2', 'email']
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'WillOs'}))
    password1 = forms.CharField(strip=False,widget=forms.PasswordInput(attrs={'placeholder': '***********'}))
    password2 = forms.CharField(strip=False,widget=forms.PasswordInput(attrs={'placeholder': '***********'}))
    email = forms.EmailField(required=True,widget=forms.TextInput(attrs={'placeholder': 'ejemplo@ejemplo.com'}))
    def __init__(self, *args, **kwargs):
        super(crearForm, self).__init__(*args, **kwargs)
        for fieldname in ['username', 'password1', 'password2', 'email']:
            self.fields[fieldname].help_text = None


#Funciones y variables
def backendEmail(mail):
    try:
        usuario = User.objects.get(email=mail)
    except:
        usuario = None
    return usuario

#Vistas
def index(request):
    if request.user.is_authenticated:
        if request.user.is_staff == 1:
            return redirect('administrar')
        usuario = cliente.objects.get(idUsuario = request.user.id)
        if usuario.capacidadFisica != 'NULO':
            return redirect('cliente')
        if request.method=="POST":
            usuario.capacidadFisica=request.POST['capacidadFisica']
            usuario.peso=request.POST['peso']
            usuario.estatura=request.POST['estatura']
            usuario.save()
            return redirect('cliente')
    else:
        usuario = None
    return render(request, 'appPocketTrainer\index.html',{'cliente':usuario})

def registro(request):
    if request.user.is_authenticated:
        if request.user.is_staff == 1:
            return redirect('administrar')
        return redirect('index')
    else:
        crearUsuario = crearForm()
        if request.method=="POST":
            crearUsuario = crearForm(request.POST)
            if crearUsuario.is_valid():
                try:
                    crearUsuario.save()
                    usuario = cliente(
                        idUsuario = int(User.objects.get(username=request.POST['username']).pk),
                        nombre = request.POST['nombre']+" "+request.POST['apP']+" "+request.POST['apM'],
                        capacidadFisica = 'NULO',
                        peso = 0,
                        estatura = 0
                    )
                    usuario.save()
                except:
                    print('Errores en la creacion de usuario')
            else:
                print('Error de formulario')
    return render(request, "appPocketTrainer/registro.html",{'crearUsuario':crearUsuario})

def login(request):
    if request.user.is_authenticated:
        if request.user.is_staff == 1:
            return redirect('administrar')
        return redirect('index')
    else:
        if request.method=='POST':
            usuarioValidar = backendEmail(request.POST['credencial'])
            if usuarioValidar is not None:
                usuario = authenticate(request, username=usuarioValidar.username, password=request.POST['password'])
            else:
                usuario = authenticate(request, username=request.POST['credencial'], password=request.POST['password'])
            
            if usuario is not None:
                auth_login(request, usuario)
                return redirect('index')
            else:
                return redirect('login')
    return render(request, "appPocketTrainer/login.html")

def recuperarPassword(request):
    if request.user.is_authenticated:
        if request.user.is_staff == 1:
            return redirect('administrar')
        return redirect('index')
    else:
        if request.method=='POST':
            if(backendEmail(request.POST['email'])):
                #Encriptación
                mailUsuario = request.POST['email']
                llave = Fernet.generate_key()
                fernet = Fernet(llave)
                encriptado = fernet.encrypt(mailUsuario.encode())
                link = "http://localhost:8000/nuevapass/"+encriptado.decode('utf-8')+"/"+llave.decode('utf-8')+"/"

                #Envío de mail
                send_mail(
                    'Cambio de contraseña Pocket Trainer',
                    'Hola, se nos ha un notificado de un cambio de contraseña para esta cuenta, en ese caso presiona el link\nSi no fuiste tu quien solicitó el cambio de contraseña ignora el siguiente link\n'+link,
                    'no-replypockettrainer@outlook.com',
                    [mailUsuario],
                    fail_silently=False
                )
                messages.success(request, "Se envió un correo electrónico al usuaio especificado")
            else:
                messages.error(request,"Mail de usuario no encontrado")
    return render(request, "appPocketTrainer/recuperar.html")

def nuevaPass(request, email, llave):
    if(request.method == 'POST'):
        #Desencriptacion
        fernet = Fernet(llave.encode('utf-8'))
        mail = fernet.decrypt(email.encode('utf-8')).decode()
        
        #Obtención de usuario
        try:
            usuario = User.objects.get(email=mail)
            usuario.set_password(request.POST['password1'])
            usuario.save()
            messages.success(request, 'Cambio de contraseña exitoso')
            return redirect('recuperar')
        except:
            messages.error(request, 'Error al cambiar contraseña, intentelo de nuevo')
            return redirect('recuperar')
    return render(request, "appPocketTrainer/nuevapass.html")

@login_required(login_url='login')
def logout(request):
    funcLogout(request)
    return redirect('login')

@login_required(login_url='login')
def nuevaRutina(request):
    listaEjercicios = ejercicios.objects.all()
    if request.user.is_staff == 1:
        return redirect('administrar')
    if request.method == 'POST':
        #Inicializar Rutina
        usuario = cliente.objects.get(idUsuario = request.user.id)
        rutina = perfilRutina(
            nombre=request.POST['nombre'],
            usuario=usuario,
            dias = request.POST.getlist('dias')
        )
        rutina.save()
        #Inicializar Ejercicios de rutina
        listaEjer = request.POST.getlist('id_ejercicio')
        listaDur = request.POST.getlist('duracion_ejercicio')
        for i in range(len(listaEjer)):
            ejerRut = ejerciciosRutina(
                rutina = rutina,
                ejercicio = ejercicios.objects.get(id = listaEjer[i]),
                duracion = listaDur[i]
            )
            ejerRut.save()

    return render(request, "appPocketTrainer/rutina.html",{'ejercicios':listaEjercicios})

@login_required(login_url='login')
def paginaCliente(request):
    #Inicialización de variables
    usuario = cliente.objects.get(idUsuario = request.user.id)
    rutinas = perfilRutina.objects.filter(usuario = usuario.id)
    semana = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo']
    hoy = datetime.datetime.today().weekday()
    rutinaHoy = []

    #Escritura del calendario
    tablas = ""
    for index, semana in enumerate(semana):
        tablas = tablas + """
        <table class="ejercicios" id="""+str(index)+""">
            <tr>
                <th>
        """+semana+"""
                </th>
            </tr>
        """
        for rutina in rutinas:
            diasRutina = list(rutina.dias)
            if str(index) in diasRutina:
                tablas = tablas + """
                    <tr>
                        <th>
                """+rutina.nombre+"""
                        </th>
                    </tr>
                """
        tablas = tablas + "</table><br>"
    
    #Determinar las rutinas de hoy
    for rutina in rutinas:
        diasRutina = list(rutina.dias)
        if str(hoy) in diasRutina:
            rutinaHoy.append(rutina)

    #En caso si el usuario aun no registra capacidadFisica
    if usuario.capacidadFisica == 'NULO':
        return redirect('index')
    return render(request, "appPocketTrainer/cliente.html",{'cliente':usuario,'tabla':tablas, 'rutinas':rutinas,'rutinaHoy':rutinaHoy})

@login_required(login_url='login')
def eliminarRutina(request, idrutina):
    usuario = cliente.objects.get(idUsuario = request.user.id)
    try:
        perfilRutina.objects.get(id=idrutina, usuario=usuario.id).delete()
        return redirect('cliente')
    except:
        return redirect('cliente')

@login_required(login_url='login')
def misDatos(request):
    usuario = cliente.objects.get(idUsuario = request.user.id)
    if usuario.capacidadFisica == 'NULO':
        return redirect('index')
    return render(request, "appPocketTrainer/misdatos.html",{'cliente':usuario})

@login_required(login_url='login')
def miRutina(request, idrutina):
    usuario = cliente.objects.get(idUsuario = request.user.id)
    if usuario.capacidadFisica == 'NULO':
        return redirect('index')
    try:
        rutina = perfilRutina.objects.get(id=idrutina, usuario=usuario.id)
        listaRutina = ejerciciosRutina.objects.filter(rutina=idrutina)
        return render(request, "appPocketTrainer/datosRutina.html",{'rutina':rutina,'ejercicios':listaRutina})
    except:
        return redirect('cliente')

#Administración
@staff_member_required(login_url='login')
def admninistracion(request):
    lista = User.objects.all()
    if(request.method == 'POST'):
        ejercicio = ejercicios(
            tipo = request.POST['nombre'].upper(),
            archivo =  "DEFAULT"
        )
        ejercicio.save()
    return render(request, "appPocketTrainer/admin.html",{'listaUsuarios':lista})