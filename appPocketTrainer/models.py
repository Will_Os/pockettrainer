from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

class cliente(models.Model):
    idUsuario = models.IntegerField()
    nombre = models.CharField(max_length=256, default='default')
    capacidadFisica = models.CharField(max_length=128)
    peso = models.FloatField()
    estatura = models.FloatField()

class perfilRutina(models.Model):
    nombre = models.CharField(max_length=256)
    usuario = models.ForeignKey(cliente, on_delete=models.CASCADE)
    dias = models.CharField(max_length=20)

class ejercicios(models.Model):
    tipo = models.CharField(max_length=128)
    archivo = models.CharField(max_length=255)

class ejerciciosRutina(models.Model):
    rutina = models.ForeignKey(perfilRutina, on_delete=models.CASCADE)
    ejercicio = models.ForeignKey(ejercicios, on_delete=models.CASCADE)
    duracion = models.FloatField()

class implementos(models.Model):
    tipo = models.CharField(max_length=128)
    cliente = models.ForeignKey(cliente, on_delete=models.CASCADE)


